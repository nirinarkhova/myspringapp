package ru.nirinarkhova.myspringapp.model;

public class Champion {
    private int id;

    private String name;

    private String chempOrigin;

    private String chempClass;

    private int cost;

    public Champion() {
    }

    public Champion(int id, String name, String chempOrigin, String chempClass, int cost) {
        this.id = id;
        this.name = name;
        this.chempOrigin = chempOrigin;
        this.chempClass = chempClass;
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChempOrigin() {
        return chempOrigin;
    }

    public void setChempOrigin(String chempOrigin) {
        this.chempOrigin = chempOrigin;
    }

    public String getChempClass() {
        return chempClass;
    }

    public void setChempClass(String chempClass) {
        this.chempClass = chempClass;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
