package ru.nirinarkhova.myspringapp.dao;

import org.springframework.stereotype.Component;
import ru.nirinarkhova.myspringapp.model.Champion;

import java.util.ArrayList;
import java.util.List;

@Component
public class ChampionDAO {

    private List<Champion> champion;
    private static  int STATION_COUNT;

    {
        champion = new ArrayList<>();

        champion.add(new Champion(++STATION_COUNT, "Ahri", "Syndicate", "Arcanist", 4));
        champion.add(new Champion(++STATION_COUNT, "Brand", "Debonair", "Arcanist", 1));
        champion.add(new Champion(++STATION_COUNT, "Braum", "Syndicate", "Bodyguard", 4));
        champion.add(new Champion(++STATION_COUNT, "Corki", "Yordle", "Twinshot", 2));
        champion.add(new Champion(++STATION_COUNT, "Irelia", "Scrap", "Striker", 4));
        champion.add(new Champion(++STATION_COUNT, "Lulu", "Yordle", "Enchanter", 2));
    }

    public List<Champion> index() {
        return champion;
    }

    public Champion show(int id) {
        return champion.stream().filter(stations -> stations.getId() == id).findAny().orElse(null);
    }

    public void save(Champion addChampion) {
        addChampion.setId(++STATION_COUNT);
        champion.add(addChampion);
    }

    public void update(int id, Champion champion) {
        Champion championToBeUpdate = show(id);
        championToBeUpdate.setName(champion.getName());
        championToBeUpdate.setChempClass(champion.getChempClass());
        championToBeUpdate.setChempOrigin(champion.getChempOrigin());
        championToBeUpdate.setCost(champion.getCost());
    }

    public void delete(int id) {
        champion.removeIf(s -> s.getId() == id);
    }
}
