package ru.nirinarkhova.myspringapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.nirinarkhova.myspringapp.dao.ChampionDAO;
import ru.nirinarkhova.myspringapp.model.Champion;

@Controller
@RequestMapping("/champions")
public class ChampionsController {

    private ChampionDAO championDAO;

    @Autowired
    public ChampionsController(ChampionDAO stationsDAO){
        this.championDAO = stationsDAO;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("champions", championDAO.index());
        return "champions/allChampions";
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") int id, Model model) {
        model.addAttribute("champions", championDAO.show(id));
        return "champions/show";
    }

    @GetMapping("/new")
    public String newChampion(@ModelAttribute("champions") Champion champion) {
        return "champions/new";
    }

    @PostMapping()
    public String create(@ModelAttribute("champions") Champion champion) {
        championDAO.save(champion);
        return "redirect:/champions";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @ModelAttribute("id") int id) {
        model.addAttribute("champions", championDAO.show(id));
        return "champions/edit";
    }

    @PatchMapping("/{id}")
    public String update(@ModelAttribute("champions") Champion champion, @ModelAttribute("id") int id) {
        championDAO.update(id, champion);
        return "redirect:/champions";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        championDAO.delete(id);
        return "redirect:/champions";
    }

    @GetMapping("/findChampions")
    public String findChampion(@ModelAttribute("book") Champion champion) {
        return "champions/findChampions";
    }

}
